<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencia_profesores', function (Blueprint $table) {
            $table->id();
            $table->integer('idProfesor');
            $table->date('fecha');
            $table->time('hora_entrada');
            $table->time('hora_asistencia');
            $table->integer('retardo');
            $table->integer('minutos_retardo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencia_profesores');
    }
};
