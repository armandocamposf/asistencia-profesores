<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHorarioProfesoresRequest;
use App\Http\Requests\UpdateHorarioProfesoresRequest;
use App\Models\HorarioProfesores;

class HorarioProfesoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreHorarioProfesoresRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHorarioProfesoresRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HorarioProfesores  $horarioProfesores
     * @return \Illuminate\Http\Response
     */
    public function show(HorarioProfesores $horarioProfesores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HorarioProfesores  $horarioProfesores
     * @return \Illuminate\Http\Response
     */
    public function edit(HorarioProfesores $horarioProfesores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateHorarioProfesoresRequest  $request
     * @param  \App\Models\HorarioProfesores  $horarioProfesores
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHorarioProfesoresRequest $request, HorarioProfesores $horarioProfesores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HorarioProfesores  $horarioProfesores
     * @return \Illuminate\Http\Response
     */
    public function destroy(HorarioProfesores $horarioProfesores)
    {
        //
    }
}
