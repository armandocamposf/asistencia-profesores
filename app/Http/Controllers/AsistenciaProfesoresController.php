<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAsistenciaProfesoresRequest;
use App\Http\Requests\UpdateAsistenciaProfesoresRequest;
use App\Models\AsistenciaProfesores;
use Illuminate\Support\Facades\Auth;

class AsistenciaProfesoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user() != null) {
            return view('modulos.asistenciasModule');
        } else {
            return redirect()->route('index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAsistenciaProfesoresRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAsistenciaProfesoresRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AsistenciaProfesores  $asistenciaProfesores
     * @return \Illuminate\Http\Response
     */
    public function show(AsistenciaProfesores $asistenciaProfesores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AsistenciaProfesores  $asistenciaProfesores
     * @return \Illuminate\Http\Response
     */
    public function edit(AsistenciaProfesores $asistenciaProfesores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAsistenciaProfesoresRequest  $request
     * @param  \App\Models\AsistenciaProfesores  $asistenciaProfesores
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAsistenciaProfesoresRequest $request, AsistenciaProfesores $asistenciaProfesores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AsistenciaProfesores  $asistenciaProfesores
     * @return \Illuminate\Http\Response
     */
    public function destroy(AsistenciaProfesores $asistenciaProfesores)
    {
        //
    }
}
