<?php

namespace App\Http\Controllers;

use App\Models\Profesores;
use Illuminate\Support\Facades\Auth;


class ProfesoresController extends Controller
{

    public function index()
    {
        if (Auth::user() != null) {
            return view('modulos.profesoresModule');
        } else {
            return redirect()->route('index');
        }
    }

}
