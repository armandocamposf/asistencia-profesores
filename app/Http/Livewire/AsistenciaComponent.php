<?php

namespace App\Http\Livewire;

use App\Models\AsistenciaProfesores;
use App\Models\HorarioProfesores;
use App\Models\Profesores;
use Livewire\Component;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class AsistenciaComponent extends Component
{
    use LivewireAlert;

    public $vista = "asistencia", $display = "none";

    public $codeProfesor, $asistencias, $count, $retardos, $fecha_inicio, $fecha_fin, $suma, $searchs = [];

    public function render()
    {
        $this->asistencias = AsistenciaProfesores::whereFecha(date('Y-m-d'))->with('profesor')->get();
        return view('livewire.asistencia-component');
    }

    public function asistencias()
    {
        $this->vista = "asistencia";
    }

    public function asistencia()
    {
        $profesor = Profesores::where('codigo', $this->codeProfesor)->first();

        if ($profesor != null) {
            $dia = date('w');
            $horarios = HorarioProfesores::whereDia($dia)->where('idProfesor', $profesor->id)->get();


            foreach ($horarios as $horario) {
                if (now()->toTimeString() >= $horario->hora_inicio && now()->toTimeString() <= $horario->hora_fin) {
                    $asistencia = AsistenciaProfesores::where('hora_entrada', $horario->hora_inicio)
                        ->where('idProfesor', $profesor->id)
                        ->where('fecha', date('Y-m-d'))
                        ->count();

                    if ($asistencia > 0) {
                        $this->alert('error', 'Este profesor ya se le registro asistencia, en este horario!', [
                            'position' => 'center',
                            'timer' => 3000,
                            'toast' => true,
                            'width' => '600',
                        ]);
                    } else {
                        $carbon1 = new \Carbon\Carbon(date('Y-m-d') . " " . $horario->hora_inicio);
                        $carbon2 = new \Carbon\Carbon(date('Y-m-d') . " " . date('H:i:s'));
                        $minutesDiff = $carbon1->diffInMinutes($carbon2);

                        if ($minutesDiff > 0) {
                            $retardo = 1;
                        } else {
                            $retardo = 2;
                        }

                        AsistenciaProfesores::create([
                            'idProfesor' => $profesor->id,
                            'fecha' => date('Y-m-d'),
                            'hora_entrada' => $horario->hora_inicio,
                            'hora_asistencia' => date('H:i:s'),
                            'retardo' => $retardo,
                            'minutos_retardo' => $minutesDiff
                        ]);

                        $this->alert('success', 'Asistencia Registrada!', [
                            'position' => 'center',
                            'timer' => 3000,
                            'toast' => true,
                            'width' => '600',
                            'text' => 'El usuario tiene ' . $minutesDiff . " minutos de retardo. ",
                        ]);
                    }
                } else {
                    $this->alert('error', 'Este Profesor no tiene horario asignado, en este bloque!', [
                        'position' => 'center',
                        'timer' => 3000,
                        'toast' => true,
                        'width' => '600',
                    ]);
                }
            }
        } else {
            $this->alert('error', 'Profesor No Existe!', [
                'position' => 'center',
                'timer' => 3000,
                'toast' => true,
                'width' => '600',
            ]);
        }

        $this->asistencias = AsistenciaProfesores::whereFecha(date('Y-m-d'))->with('profesor')->get();
        $this->codeProfesor = "";
    }

    public function search()
    {
        $this->vista = "search";
    }

    public function ejecutar()
    {
        $profesor = Profesores::where('codigo', $this->codeProfesor)->first();
        $asistencias = AsistenciaProfesores::where('idProfesor', $profesor->id)
            ->whereBetween('fecha', [$this->fecha_inicio, $this->fecha_fin])
            ->get();

        $this->count = $asistencias->count();
        $this->suma = $asistencias->sum('minutos_retardo');
        $this->retardos = AsistenciaProfesores::where('idProfesor', $profesor->id)
            ->where('retardo', 1)
            ->whereBetween('fecha', [$this->fecha_inicio, $this->fecha_fin])
            ->count();

        $this->searchs = $asistencias;
        $this->display = "block";
    }
}
