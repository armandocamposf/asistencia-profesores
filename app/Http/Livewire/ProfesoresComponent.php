<?php

namespace App\Http\Livewire;

use App\Models\HorarioProfesores;
use App\Models\Profesores;
use Livewire\Component;

use Jantinnerezo\LivewireAlert\LivewireAlert;

class ProfesoresComponent extends Component
{
    use LivewireAlert;

    public $vista = "list";
    public $codigo, $nombre, $idEditando, $dia, $hora_entrada, $hora_salida, $horarios = [];
    public function render()
    {
        $profesores = Profesores::all();
        return view('livewire.profesores-component', compact('profesores'));
    }

    public function profesores()
    {
        $this->codigo = "";
        $this->nombre = "";
        $this->vista = "list";
    }

    public function newUser()
    {
        $this->vista = "create";
    }

    public function save()
    {
        $profesor = Profesores::create([
            'codigo' => $this->codigo,
            'nombre' => $this->nombre
        ]);

        $this->profesores();
        $this->alert('success', 'Profesor Creado!');
    }

    public function update()
    {
        $profesor = Profesores::find($this->idEditando);
        $profesor->codigo = $this->codigo;
        $profesor->nombre = $this->nombre;
        $profesor->save();

        $this->profesores();
        $this->alert('success', 'Profesor Actualizado!');
    }

    public function edit($idProfesor)
    {
        $profesor = Profesores::find($idProfesor);
        $this->codigo = $profesor->codigo;
        $this->nombre = $profesor->nombre;
        $this->idEditando = $profesor->id;

        $this->vista = "edit";

    }

    public function horariosProfesor($idProfesor)
    {
        $profesor = Profesores::find($idProfesor);
        $this->idEditando = $profesor->id;
        $this->horarios = HorarioProfesores::where('idProfesor', $this->idEditando)->get();
        $this->vista = 'horarios';
    }

    public function horarios()
    {
        $this->dia = 0;
        $this->hora_entrada = "";
        $this->hora_salida = "";
    }

    public function crearHorario()
    {
        $horario = HorarioProfesores::create([
            'idProfesor' => $this->idEditando,
            'dia' => $this->dia,
            'hora_inicio' => $this->hora_entrada,
            'hora_fin' => $this->hora_salida
        ]);

        $this->horarios();

        $this->alert('success', 'Horario Agregado!');
        $this->horarios = HorarioProfesores::where('idProfesor', $this->idEditando)->get();
    }

    public function eliminarHorario($idHorario)
    {
        HorarioProfesores::destroy($idHorario);
        $this->alert('success', 'Horario Eliminado con exito!');
        $this->horarios = HorarioProfesores::where('idProfesor', $this->idEditando)->get();
    }
}
