<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class UsuariosComponent extends Component
{
    public $vista = "list", $ancho = 12;

    public $nombre, $usuario, $clave, $rol_id, $idEditando;


    public function user()
    {
        $this->vista = 'list';
    }

    public function render()
    {
        $usuarios = User::all();

        return view('livewire.usuarios-component', compact('usuarios'));
    }

    public function edit($idUsuario)
    {
        $usuario = User::find($idUsuario);
        $this->nombre = $usuario->name;
        $this->rol_id = $usuario->rol_id;
        $this->usuario = $usuario->usuario;
        $this->idEditando = $usuario->id;
        $this->vista = 'edit';
    }

    public function update()
    {
        $usuario = User::find($this->idEditando);
        $usuario->name = $this->nombre;
        if($this->clave != null)
        {
            $usuario->password = sha1($this->clave);
        }
        $usuario->rol_id = $this->rol_id;
        $usuario->usuario = $this->usuario;
        $usuario->save();

        $this->vista = 'list';
    }

    public function newUser()
    {
        $this->vista = "create";
    }

    public function save()
    {
        $user = User::create([
            'usuario' => $this->usuario,
            'password' => sha1($this->clave),
            'name' => $this->nombre,
            'rol_id' => $this->rol_id
        ]);

        $this->vista = 'list';
    }
}
