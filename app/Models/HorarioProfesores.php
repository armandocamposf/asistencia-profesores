<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HorarioProfesores extends Model
{
    use HasFactory;

    protected $fillable = [
        'idProfesor',
        'dia',
        'hora_inicio',
        'hora_fin',
    ];
}
