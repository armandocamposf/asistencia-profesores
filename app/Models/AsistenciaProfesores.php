<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsistenciaProfesores extends Model
{
    use HasFactory;

    protected $fillable = [
        'idProfesor',
        'fecha',
        'hora_entrada',
        'hora_asistencia',
        'retardo',
        'minutos_retardo',
    ];

    public function profesor()
    {
        return $this->hasOne(Profesores::class, 'id', 'idProfesor');
    }
}
