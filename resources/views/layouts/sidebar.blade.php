<aside class="main-sidebar fixed offcanvas shadow" data-toggle='offcanvas'>
    <section class="sidebar">
        <div class="w-80px mt-3 mb-3 ml-3">
            <img src="assets/img/basic/logo.png" alt="">
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
                aria-controls="userSettingsCollapse"
                class="btn-fab btn-fab-sm absolute fab-right-bottom fab-top btn-primary shadow1 ">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src="assets/img/dummy/u2.png" alt="User Image">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">{{ Auth::user()->name }}</h6>
                        <a href="#"><i class="icon-circle text-primary blink"></i> En Linea</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        <a href="#" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-umbrella text-blue"></i>Cambiar Clave</a>
                        <a href="{{ route('logout') }}" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-security text-purple"></i>Cerrar Sesion</a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header"><strong>MENU</strong></li>
            @if (Auth::user()->rol_id == 1)
                <li><a href="{{ route('usuarios') }}"><i
                            class="icon icon-account_box light-green-text s-18"></i>Usuarios</a>
                <li><a href="{{ route('profesores') }}"><i class="icon icon-users text-danger s-18"></i>Profesores</a>
            @endif
            <li><a href="{{ route('asistencias') }}"><i
                        class="icon icon-note-checked2 text-info s-18"></i>Asistencia</a>
            </li>
        </ul>
    </section>
</aside>
