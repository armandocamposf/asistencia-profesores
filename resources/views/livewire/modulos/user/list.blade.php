    <div class="row my-3 ml-3 mr-3">
        <div class="col-md-{{$ancho}}">
            <div class="card r-0 shadow">
                <div class="table-responsive">
                    <form>
                        <table class="table table-striped table-hover r-0">
                            <thead>
                                <tr class="no-b">
                                    <th>Nombre</th>
                                    <th>Usuario</th>
                                    <th>Rol</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse ($usuarios as $item)
                                    <tr>

                                        <td>
                                            <div class="avatar avatar-md mr-3 mt-1 float-left">
                                                <span class="avatar-letter avatar-letter-a  avatar-md circle"></span>
                                            </div>
                                            <div>
                                                <div>
                                                    <strong>{{ $item->name }}</strong>
                                                </div>
                                            </div>
                                        </td>

                                        <td>{{ $item->usuario }}</td>
                                        <td>
                                            @if ($item->rol_id == 1)
                                                <span class="r-3 badge badge-success ">Administrator</span>
                                            @else
                                                <span class="r-3 badge badge-warning ">Secretari@</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->usuario != 'admin')
                                            <i class="s-24 icon-pencil-square text-success" wire:click='edit({{$item->id}})' style="font-size: 30px"></i>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <tr class="text-center">
                                        <td colspan="4" class="py-3 italic">No hay información</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>


    </div>
