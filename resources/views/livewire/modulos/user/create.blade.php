<div class="container-fluid">
        <div class="row my-3">
            <div class="col-md-10  offset-md-1">
                <form action="#">
                    <div class="card no-b  no-r">
                        <div class="card-body">
                            <h5 class="card-title">Crear Usuario</h5>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Nombre</label>
                                        <input id="name" placeholder="Nombre"
                                            class="form-control r-0 light s-12 " wire:model='nombre' type="text">
                                    </div>
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Usuario</label>
                                        <input id="name" placeholder="Usuario" wire:model='usuario' class="form-control r-0 light s-12 " type="text">
                                    </div>
                                    <div class="form-group m-0">
                                        <label for="name" class="col-form-label s-12">Clave</label>
                                        <input id="name" placeholder="Clave" wire:model='clave' class="form-control r-0 light s-12 " type="text">
                                    </div>
                                    <div class="form-group m-0">
                                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Tipo de Usuario</label>
                                        <select wire:model='rol_id' class="custom-select my-1 mr-sm-2 form-control r-0 light s-12" id="inlineFormCustomSelectPref">
                                            <option selected> -- SELECCIONE --</option>
                                            <option value="1">ADMINISTRADOR</option>
                                            <option value="2">SECRETARI@</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <button type="button" wire:click='save()' class="btn btn-primary w-100"><i class="icon-save mr-2"></i>Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</div>
