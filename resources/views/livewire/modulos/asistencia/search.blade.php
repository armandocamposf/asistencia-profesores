<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-12">
            <form action="#">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <div class="row d-flex p-3">
                            <h5 class="card-title">Registrar Asistencia</h5>

                        </div>


                        <div class="form-row mt-2">
                            <div class="col-md-3">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Codigo Profesor</label>
                                    <input type="text" wire:model='codeProfesor' class="form-control r-0 light s-12">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Fecha Inicio</label>
                                    <input type="date" wire:model='fecha_inicio' class="form-control r-0 light s-12">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Fecha Final</label>
                                    <input type="date" wire:model='fecha_fin' class="form-control r-0 light s-12">
                                </div>
                            </div>
                            <div class="col-md-3 d-flex">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12 text-white">.</label>
                                    <button type="button" wire:click='ejecutar()'
                                        class="btn btn-success btn-sm w-100">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body" style="display: {{ $display }}">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <div class="bg-primary text-white lighten-2">
                                            <div class="pt-2 pb-2 pl-5 pr-5">
                                                <h5 class="font-weight-normal s-14">Registros de Asistencia</h5>
                                                <span class="s-48 font-weight-lighter text-primary">
                                                    <small></small>{{ $count }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <div class="bg-warning text-white lighten-2">
                                            <div class="pt-2 pb-2 pl-5 pr-5">
                                                <h5 class="font-weight-normal s-14">Total Retardos</h5>
                                                <span class="s-48 font-weight-lighter text-primary">
                                                    <small></small>{{ $retardos }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-body p-0">
                                        <div class="bg-danger text-white lighten-2">
                                            <div class="pt-2 pb-2 pl-5 pr-5">
                                                <h5 class="font-weight-normal s-14">Minutos Retardos</h5>
                                                <span class="s-48 font-weight-lighter text-primary">
                                                    <small></small>{{ $suma }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-hover mt-3">
                            <tbody>
                                <tr>
                                    <th>Hora Entrada</th>
                                    <th>Hora Asistenca</th>
                                    <th>Minutos Retrasos</th>
                                    <th></th>
                                </tr>
                                @if($searchs != null)
                                @forelse ($searchs as $asistencia)
                                    <tr>
                                        <td>{{ $asistencia->hora_entrada }}</td>
                                        <td>{{ $asistencia->hora_asistencia }}</td>
                                        <td>{{ $asistencia->minutos_retardo }} MIN</td>
                                        <td>
                                            @switch($asistencia->retardo)
                                                @case(1)
                                                    <span class="r-3 badge badge-danger ">Retardado</span>
                                                @break

                                                @case(2)
                                                    <span class="r-3 badge badge-success ">A Tiempo</span>
                                                @break
                                            @endswitch
                                        </td>
                                    </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="3" class="py-3 italic">No hay información</td>
                                        </tr>
                                    @endforelse
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
