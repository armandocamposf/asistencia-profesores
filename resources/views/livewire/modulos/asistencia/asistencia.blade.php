<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-12">
            <form action="#">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <div class="row d-flex p-3">
                            <h5 class="card-title">Registrar Asistencia</h5>

                        </div>


                        <div class="form-row mt-2">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Codigo Profesor</label>
                                    <input type="text" wire:model='codeProfesor' class="form-control r-0 light s-12">
                                </div>
                            </div>
                            <div class="col-md-2 d-flex">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12 text-white">.</label>
                                    <button type="button" wire:click='asistencia()'
                                        class="btn btn-success btn-sm w-100">Registrar Asistencia</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Profesor</th>
                                    <th>Hora Entrada</th>
                                    <th>Hora Asistenca</th>
                                    <th>Minutos Retrasos</th>
                                    <th></th>
                                </tr>
                                @foreach ($asistencias as $asistencia)
                                    <tr>
                                            <td>{{$asistencia->profesor->nombre}}</td>
                                            <td>
                                                <span class="badge badge-success" style="font-size: 12px;"> <i class="s-12 icon-timer"></i> {{$asistencia->hora_entrada}}</span>
                                                </td>
                                            <td>
                                            @switch($asistencia->retardo)
                                                @case(1)
                                                    <span class="badge badge-danger" style="font-size: 12px;"> <i class="s-12 icon-timer"></i> {{$asistencia->hora_asistencia}}</span>
                                                @break
                                                @case(2)
                                                    <span class="badge badge-success" style="font-size: 12px;"> <i class="s-12 icon-timer"></i> {{$asistencia->hora_asistencia}}</span>
                                                @break
                                            @endswitch
                                            </td>
                                            <td>{{$asistencia->minutos_retardo}} MIN</td>
                                            <td>
                                            @switch($asistencia->retardo)
                                                @case(1)
                                                    <span class="r-3 badge badge-danger ">Retardado</span>
                                                @break
                                                @case(2)
                                                    <span class="r-3 badge badge-success ">A Tiempo</span>
                                                @break
                                            @endswitch
                                            </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
