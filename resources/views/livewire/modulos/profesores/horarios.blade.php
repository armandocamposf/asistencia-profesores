<div class="container-fluid">
    <div class="row my-3">
        <div class="col-md-12">
            <form action="#">
                <div class="card no-b  no-r">
                    <div class="card-body">
                        <div class="row d-flex p-3">
                            <h5 class="card-title">Crear Profesor</h5>
                            <button wire:click='profesores()' class="ml-auto btn btn-warning">Regresar</button>
                        </div>


                        <div class="form-row mt-2">
                            <div class="col-md-3">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Dia</label>
                                    <select name="dia" wire:model='dia' class="form-control r-0 light s-12" id="">
                                        <option value="">-- SELECCIONE --</option>
                                        <option value="0">DOMINGO</option>
                                        <option value="1">LUNES</option>
                                        <option value="2">MARTES</option>
                                        <option value="3">MIERCOLES</option>
                                        <option value="4">JUEVES</option>
                                        <option value="5">VIERNES</option>
                                        <option value="6">SABADO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Hora Entrada</label>
                                    <input type="time" name="" class="form-control r-0 light s-12" wire:model='hora_entrada' id="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12">Hora Salida</label>
                                    <input type="time" name="" class="form-control r-0 light s-12" wire:model='hora_salida' id="">
                                </div>
                            </div>
                            <div class="col-md-2 d-flex">
                                <div class="form-group m-0">
                                    <label for="name" class="col-form-label s-12 text-white">.</label>
                                <button type="button" wire:click='crearHorario()' class="btn btn-success btn-sm w-100">Agregar Horario</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Dia</th>
                                    <th>Hora Entrada</th>
                                    <th>Hora Salida</th>
                                    <th></th>
                                </tr>
                                @foreach ($horarios as $horario)
                                <tr>
                                    <td>
                                            @switch ($horario->dia)
                                            @case (0)
                                            Domingo
                                            @break
                                            @case (1)
                                            Lunes
                                            @break
                                            @case (2)
                                            Martes
                                            @break
                                            @case (3)
                                            Miercoles
                                            @break
                                            @case (4)
                                            Jueves
                                            @break
                                            @case (5)
                                            Viernes
                                            @break
                                            @case (6)
                                            Sabado
                                            @break
                                        @endswitch
                                    </td>
                                    <td>
                                        <span class="badge badge-success" style="font-size: 12px;"> <i class="s-12 icon-timer"></i> {{$horario->hora_inicio}}</span>
                                        </td>
                                    <td>
                                        <span class="badge badge-danger" style="font-size: 12px;"> <i class="s-12 icon-timer"></i> {{$horario->hora_fin}}</span>
                                        </td>
                                        </td>
                                    <td>
                                        <i wire:click='eliminarHorario({{$horario->id}})' class="s-24 icon-trash-can4 text-danger"></i>
                                    </td>
                                    <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
