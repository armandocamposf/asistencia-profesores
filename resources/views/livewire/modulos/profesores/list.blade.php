<div class="row my-3 ml-3 mr-3">
    <div class="col-md-12">
        <div class="card r-0 shadow">
            <div class="table-responsive">
                <form>
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>Codigo</th>
                                <th>Profesor</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($profesores as $item)
                            <tr>
                                <td>{{ $item->codigo }}</td>
                                <td>
                                    <div class="avatar avatar-md mr-3 mt-1 float-left">
                                        <span class="avatar-letter avatar-letter-a  avatar-md circle"></span>
                                    </div>
                                    <div>
                                        <div>
                                            <strong>{{ $item->nombre }}</strong>
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <i class="s-24 icon-pencil-square text-success" wire:click='edit({{$item->id}})'
                                        style="font-size: 30px"></i>
                                        <i class="s-24 icon-note-list2 text-danger" wire:click='horariosProfesor({{$item->id}})'></i>
                                </td>
                            </tr>
                            @empty
                            <tr class="text-center">
                                <td colspan="3" class="py-3 italic">No hay información</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>


</div>
