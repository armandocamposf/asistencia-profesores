<div>
    <div class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        ASISTENCIA
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <span class="nav-link" wire:click="asistencias()"><i
                                class="icon icon-home2"></i>Asistencia</span>
                    </li>

                    <li>
                        <span class="nav-link" wire:click='search()'><i class="icon icon-user-plus"></i>Buscar Asistencias Profesor</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include("livewire.modulos.asistencia.$vista")
</div>
