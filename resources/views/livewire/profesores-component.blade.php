<div>
    <div class="blue accent-3 relative nav-sticky">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-box"></i>
                        PROFESORES
                    </h4>
                </div>
            </div>
            <div class="row">
                <ul class="nav responsive-tab nav-material nav-material-white">
                    <li>
                        <span class="nav-link" wire:click="profesores()"><i class="icon icon-home2"></i>Profesores</span>
                    </li>

                    <li>
                        <span class="nav-link" wire:click='newUser()'><i class="icon icon-user-plus"></i>Nuevo
                            Profesor</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include("livewire.modulos.profesores.$vista")
</div>
