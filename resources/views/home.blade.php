@extends('layouts.app')

@section('menu-superior')
    <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1">
                                    <i class="icon icon-home2"></i>Inicio</a>
@endsection

@section('contenido')
    <div class="container-fluid relative animatedParent animateOnce">
        <div class="tab-content pb-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active" id="v-pills-1">

                <div class="row">
                    <div class="col-md-12">
                        <div class="white p-5 r-5 mt-5">
                            <div class="card-title">
                                <h5> Bienvenido {{ Auth::user()->name }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
