<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    if(isset(Auth::user()->id))
    {
        return redirect()->route('home');
    }
    else
    {
        return view('welcome');
    }
})->name('index');



Route::get('home', [HomeController::class, 'home'])->name('home');
Route::post('login', [UserController::class, 'login'])->name('login');
Route::get('logout', [UserController::class, 'logout'])->name('logout');

Route::get('usuarios', [UserController::class, 'index'])->name( 'usuarios');
Route::get('profesores', [ProfesoresController::class, 'index'])->name('profesores');
Route::get('asistencias', [AsistenciaProfesoresController::class, 'index'])->name('asistencias');
